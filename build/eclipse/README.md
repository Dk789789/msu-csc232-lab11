# Eclipse
This directory should be your target for eclipse project files. To generate eclipse project files, type

```
$ cmake -G "Eclipse CDT4 - Unix Makefiles" ../..
```

at the command line prompt.
