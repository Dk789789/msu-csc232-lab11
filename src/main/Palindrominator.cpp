/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file Palindrominator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Alex Wilson <Alexander789@live.missouristate.edu>
 * @brief   Palindrominator implementation.
 */

#include "Palindrominator.h"
#include <stack>
#include <queue>
#include <string>

bool Palindrominator::isPalindrome(const std::string text) {
	
	//This code will put all of the contents of variable: text
	//to both a stack named stack and a queue named queue
	for (int x = 0; x < text.size(); x++){
		stack.push(text.at(x));
		queue.push(text.at(x));
	}

	while (!stack.empty() && !queue.empty()){
		if (stack.top() == queue.front()){
			stack.pop();
			queue.pop();
		}
		else{
			return false;
		}
	}
    return true;
}