/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file Demo.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Entry point for demo application.
 */

#include <cstdlib>
#include <iostream>
#include <queue>
#include <stack>

void demoQueuePushPopFront();
void demoStackPushPopTop();

int main(int argc, char *argv[]) {
    std::cout << "Lab 11 (Demo)" << std::endl << std::endl;

    demoQueuePushPopFront();
    demoStackPushPopTop();

    return EXIT_SUCCESS;
}

void demoQueuePushPopFront() {
    std::queue<int> myqueue;

    // Notice STL queue uses push() almost like we use enqueue; it differs in that it
    // is a void operation as opposed to returning a bool value
    for (int i=0; i<5; ++i) myqueue.push(i);

    std::cout << "myqueue contains: ";
    // Notice STL queue uses empty() just like we use isEmpty()
    while (!myqueue.empty()) {
        // Notice that the STL queue uses front() in the way we use peekFront()
        std::cout << ' ' << myqueue.front();
        // Notice that the STL queue uses pop() almost in the way we use dequeue(); it
        // differs in that it is a void operation as opposed to returning a bool value
        myqueue.pop();
    }
    std::cout << '\n';
}

void demoStackPushPopTop() {
    std::stack<int> mystack;
    // Notice STL stack uses push() almost like we use push; it differs in that it
    // is a void operation as opposed to returning a bool value
    for (int i=0; i<5; ++i) mystack.push(i);

    std::cout << "Popping out elements...";
    // Notice STL stack uses empty() just like we use isEmpty()
    while (!mystack.empty())
    {
        // Notice STL stack uses top() just like we expect
        std::cout << ' ' << mystack.top();
        // Notice STL stack uses pop() almost like we do; it differs in that it is a void
        // operation as opposed to returning a bool value
        mystack.pop();
    }
    std::cout << '\n';
}