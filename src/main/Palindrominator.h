/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file Palindrominator.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Palindrominator specification.
 */

#ifndef LAB11_PALINDROMINATOR_H
#define LAB11_PALINDROMINATOR_H

#include <queue>
#include <stack>

class Palindrominator {
public:
    /**
     * Determine whether the given string is a palindrome.
     *
     * @param text the string under interrogation
     * @return True is returned if the given text is a palindrome, false otherwise.
     */
    bool isPalindrome(const std::string text);
private:
    std::queue<char> queue;
    std::stack<char> stack;
};

#include "Palindrominator.cpp"

#endif //LAB11_PALINDROMINATOR_H
